from django.apps import AppConfig


class AppLaporanConfig(AppConfig):
    name = 'app_laporan'
