from django.urls import path
from django.conf.urls import url
from . import views
from .views import *

app_name = 'laporan'


urlpatterns = [
    path('', views.laporan, name='laporan'),
]