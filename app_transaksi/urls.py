from django.urls import path
from django.conf.urls import url
from . import views
from .views import *

app_name = 'transaksi'


urlpatterns = [
    path('', views.ttopup, name='ttopup'),
    path('daftar', views.ttopupdaftar, name='ttopupdaftar'),
]