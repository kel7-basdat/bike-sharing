from django.apps import AppConfig


class AppTransaksiConfig(AppConfig):
    name = 'app_transaksi'
