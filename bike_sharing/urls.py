"""bike_sharing URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.1/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path, include

urlpatterns = [
    path('admin/', admin.site.urls),
	path('', include('app_login.urls')),
    path('penugasan/', include('app_penugasan.urls')),
    path('acara/', include('app_acara.urls')),
    path('voucher/', include('app_voucher.urls')),
    path('pinjam/', include('app_peminjaman.urls')),
    path('stasiun/', include('app_stasiun.urls')),
    path('sepeda/', include('app_sepeda.urls')),
    path('registrasi/', include('app_registrasi.urls')),
    path('transaksi/', include('app_transaksi.urls')),
    path('enter/', include('app_enter.urls')),
    path('laporan/', include('app_laporan.urls')),

]

