from django.shortcuts import render

# Create your views here.
response = {}

from django.http import HttpResponse
def pinjam(request): 
	return render(request, 'tambahPeminjaman.html', response)

def berhasilPinjam(request): 
	return render(request, 'berhasilPinjam.html', response)

def daftarPinjam(request): 
	return render(request, 'daftarPinjam.html', response)