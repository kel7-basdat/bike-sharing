from django.urls import path
from django.conf.urls import url
from . import views
from .views import tambahtugas, daftartugas

app_name = 'app_penugasan'


urlpatterns = [
    path('add/', views.tambahtugas, name='tambah'),
    path('', views.daftartugas, name='daftar')
]