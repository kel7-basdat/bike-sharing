from django.apps import AppConfig


class AppVoucherConfig(AppConfig):
    name = 'app_voucher'
