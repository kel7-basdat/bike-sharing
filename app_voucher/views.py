from django.shortcuts import render
from django.http import HttpResponse

# Create your views here.
response = {}

def daftarVoucher(request): 
	return render(request, 'daftarVoucherAnggota.html', response)

def daftarVoucherAdmin(request): 
	return render(request, 'daftarVoucherAdmin.html', response)

def addVoucher(request): 
	return render(request, 'tambahVoucher.html', response)

def updateVoucher(request): 
	return render(request, 'updateVoucher.html', response)
	
def berhasil(request): 
	return render(request, 'berhasilVoucher.html', response)

def berhasilUpdate(request): 
	return render(request, 'berhasilUpdateVoucher.html', response)
