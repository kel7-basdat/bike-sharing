```bash
Kelompok    : 7
Kelas       : Basdat-D
Anggota     : Arif Teguh Wangi (1706040012)
              Aulia Rosyida (1706025346)
              Devin Winardi (1706039755)
              Satrio Raffani R. (1706043494)     
```

## Link Heroku
Bike Sharing<br>
https://bikesharing-kel7.herokuapp.com/

## Status Aplikasi
[![Pipeline]( https://gitlab.com/kel7-basdat/tk4/badges/master/pipeline.svg)](https://gitlab.com/kel7-basdat/tk4/commits/master)
[![Coverage](https://gitlab.com/kel7-basdat/tk4/badges/master/coverage.svg)](https://gitlab.com/kel7-basdat/tk4/index.html)


 