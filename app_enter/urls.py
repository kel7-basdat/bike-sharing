from django.urls import path
from django.conf.urls import url
from . import views
from .views import *

app_name = 'app_enter'


urlpatterns = [
    path('', views.enterlogin, name='enterlogin')
]