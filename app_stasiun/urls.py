from django.urls import path
from django.conf.urls import url
from . import views
from .views import tambahstasiun, daftarstasiun

app_name = 'app_stasiun'


urlpatterns = [
    path('tambah/', views.tambahstasiun, name='tambah'),
    path('', views.daftarstasiun, name='daftar')
]