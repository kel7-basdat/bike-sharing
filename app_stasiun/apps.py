from django.apps import AppConfig


class AppStasiunConfig(AppConfig):
    name = 'app_stasiun'
